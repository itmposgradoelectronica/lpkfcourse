\beamer@sectionintoc {1}{Machine Setup}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{The tool magazine}{5}{0}{1}
\beamer@subsectionintoc {1}{2}{CAD systems}{8}{0}{1}
\beamer@sectionintoc {2}{The CircuitPRO GUI}{9}{0}{2}
\beamer@sectionintoc {3}{How to create a new project}{10}{0}{3}
\beamer@subsectionintoc {3}{1}{Project stages}{10}{0}{3}
\beamer@subsectionintoc {3}{2}{How many layers}{12}{0}{3}
\beamer@subsectionintoc {3}{3}{Substrate}{13}{0}{3}
\beamer@subsectionintoc {3}{4}{Layer files}{14}{0}{3}
\beamer@subsubsectionintoc {3}{4}{1}{The Layers}{17}{0}{3}
\beamer@subsubsectionintoc {3}{4}{2}{The Drill Layers}{19}{0}{3}
\beamer@subsubsectionintoc {3}{4}{3}{Contour router layer}{20}{0}{3}
\beamer@subsubsectionintoc {3}{4}{4}{Global Process Settings}{22}{0}{3}
\beamer@subsectionintoc {3}{5}{Board production}{24}{0}{3}
