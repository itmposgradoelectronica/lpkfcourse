# Rapid Prototyping Tools for PCB Design  #

![Alt text](http://dangerousprototypes.com/blog/wp-content/media/2015/05/2c6f464cb14fdcbf.jpg)

This repository was created to support the course **rapid prototyping tools for PCB design**. 
During the course, some *software would be needed*, as well as some technical information. Both would be available here, first be sure to have installed the next software:

- [Eagle](https://www.autodesk.com/products/eagle/free-download)
- [LPKF](http://sagitario.itmorelia.edu.mx/gmarx/3098-circuitpro-pm-update-2-3-495.exe)


**Also plaese bring your own Laptop to work along with us**

### Information about the course ###

* Version 1
* Place: **AE Building, room AE1 and Rapid PCB Lab - SEDEAM**
* Lecturers:
	- Gerardo Marx Ch�vez-Campos
	- Juan Alfonso Salazar-Torres
*  Assistants:
	- Ing. Oscar Lobato-Nostroza	
	- Jason Ventura-Corona
---
## Course contents:

1. Basic of Printed Circuit Design with Eagle
2. PCB design
3.  Basics of Circuit Pro and Reflow Oven
4. PCB Workshop
5. Pick & Place Workshop
6. Reparing the PCB

---
# Time table by day and teams
The information here details day by day topics

- **Day 1 *Morning*: Basic of Printed Circuit Design with Eagle**
	- Installing Eagle and libraries 
	- Adding a new library, [Sparkfun library](https://github.com/sparkfun/SparkFun-Eagle-Libraries)
	- Creating a schematic, [video tutorial](https://www.youtube.com/watch?v=-U8Sh-LHh8w&t=73s) 
	- Footprints
	
- **Day 1 *After Break*: PCB design**
	- The ratnest
	- The layout
	- Back and forward annotation
	- Gerber files
	
- **Day 2 *Morning*: Basics of Circuit Pro and Reflow Oven** 
	- How to setup the LPKF machine
	- Importing Gerber files into LPKF software
	- Improving the layers
	- Milling, Drilling and Cutting tools
	- Placement 
	- How to solder a component?
	- Reflow oven for SMD soldering
	- Solder paste and its temperature curves
	
- **Day 3: PCB Workshop**
	- LPKF 			
	- Place the tools	
	- Milling the PCB	
	- Solder Mask
	- UV Curing

- **Day 4: Pick & Place**
	- Pick & place
	- Applying the solder paste
	- Using the reflow oven
	- Testing the PCB
	- Coffe break and Feedback comments
	
- **Day 5: Reparing the PCB**
	- Rework station
	- Desoldering station
	- Desoldering tweezer
	- Soldering a new component